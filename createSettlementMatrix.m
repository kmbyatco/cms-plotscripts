%create a matrix of size 'maxpoly by maxpoly' with the x axis as sinks and y axis as source, from a input vector of sink and source pairs
function [settle, settle_count] = createSettlementMatrix( pairs, maxpoly )
	% prepare sparse matrix
	settle = zeros(maxpoly,maxpoly);
	
	% get source and sink settlement indices
	_settle_index = sub2ind( size(settle), pairs(:,1), pairs(:,2) );
	
	% count number of settlement pairs 
	[ settle_count, settle_index ] = hist( _settle_index, unique(_settle_index) );
	
	% total all settlement counts into connectivity matrix
	settle(settle_index) += settle_count;
	
	% divide each entry row with its totals of the columns
	settle = bsxfun( @rdivide, settle, sum(settle) );
	
	% remove 0/0's
	settle( isnan(settle) ) = 0;
	
endfunction
