%Check if directory has the output files of a CMS run, by default, check this directory
%Return output files in a structure if inside and CMS experiment
function cms_output = getCmsProperties( varargin )
	if ( isOctave() == 0 )
		error("Not running inside octave")
	endif

	if( length(varargin) )
		tempdir_array = varargin(:);
	else
		tempdir_array = {pwd()};
	endif
		
	% parse experiment name from input directory
	[~,expt_name,~] = fileparts( tempdir_array{1} );
	cms_output.expt_name = expt_name(6:end);
	
	cms_output.con_files = [];
	cms_output.traj_files = [];
	cms_output.con_files = char(cms_output.con_files);
	cms_output.traj_files = char(cms_output.traj_files);
	for i = 1:length(tempdir_array)
	
		tempdir = tempdir_array{i};
		printf("Reading %s\n", tempdir)
		
		% check output directory
		if(	exist( [tempdir,"/output"] )==7 )
		
			% check output files
			try
				con_files = cat(1, dir([ tempdir, "/output/con_file_*"]).name );
				traj_files = cat(1, dir([ tempdir, "/output/traj_file_*"]).name );
			catch
				error("Files under %s have irregular filenames", [ "expt_", cms_output.expt_name, "/output" ] )
			end_try_catch
			
			if( ~isempty(con_files) && ~isempty(traj_files) )
				dirname = tempdir;
				
				% place output files to structure
				cms_output.con_files = [ cms_output.con_files; [ repmat( [dirname,"/output/"], size(con_files)(1), 1 ), con_files ] ];
				cms_output.traj_files = [ cms_output.traj_files; [ repmat([dirname,"/output/"], size(traj_files)(1),1), traj_files ] ];
			else
				error("Missing traj_ and con_ output files\n");
			endif
			
		else
			error("Invalid expt directory. missing %s", [ "expt_", cms_output.expt_name, "/output" ]);
		endif
	endfor
	
	tempdir = tempdir_array{1};
	% check nest_directory
	if( exist( [tempdir,"/nests"] )==7 )
		% check number of layers
		try
			all_nest_files = cat(1, dir([ tempdir, "/nests/nest*"]).name );
		catch
			error("Files under %s have irregular filenames", [ "expt_", cms_output.expt_name, "/nests" ] )
		end_try_catch
		
		if( ~isempty(all_nest_files) )
		
			% parse and place number of layers to structure
			nest_layers = max(str2double( unique(all_nest_files(:,6)) ));
			cms_output.nest_layers = nest_layers;
		
			% check and place sample input file from each layer
			cms_output.nest_files  = [];
			cms_output.nest_files = char(cms_output.nest_files);
			for i = 1:nest_layers
				layer_nest_files = dir([tempdir, "/nests/nest_", num2str(i), "_*"]);
				
				if( ~isempty(layer_nest_files) )
					cms_output.nest_files  = [ cms_output.nest_files  ; [ "nests/", layer_nest_files(1).name ] ];
				else
					warning("Missing sample input hydro file for layer %d", i);
				endif
			endfor
			
		else	
			warning("Missing sample input hydro files");
		endif
	else
		warning("Missing %s directory\n", [ "expt_", cms_output.expt_name, "/nests" ]);
	endif
	
	% Check if input folder exists from experiment name
	if( exist( [ tempdir, "/../input_", cms_output.expt_name ] ) )
		
		% check and place polygon input file
		poly_file = [ tempdir, "/../input_", cms_output.expt_name, "/poly_", cms_output.expt_name, ".xyz" ];		
		if( exist(poly_file) )
			cms_output.poly_file = poly_file;
		else	
			warning("Missing polygon input file");
		endif
		
		% check and place release template input file
		release_file = [ tempdir, "/../input_", cms_output.expt_name, "/release_", cms_output.expt_name, ".xyz" ];		
		if( exist(release_file) )
			cms_output.release_file = release_file;
		else	
			warning("Missing release template input file");
		endif
		
		% check and place nest bounds
		if( ~isempty(all_nest_files) )
		
			cms_output.xstart = [];
			cms_output.xend = [];
			cms_output.ystart = [];
			cms_output.yend = [];
			
			for i = 1:nest_layers
				xstart = NaN;
				xend = NaN;
				ystart = NaN;
				yend = NaN;
				nml_file = [ tempdir, "/../input_", cms_output.expt_name, "/nest_", num2str(i), ".nml" ];
				
				if( exist( nml_file ))
					text = fileread( nml_file );
					
					xstart = str2double( regexp( text, '(?<=xstart\=)\s*\d+\.\d+', 'match' ) );
					xend = str2double( regexp( text, '(?<=xend\=)\s*\d+\.\d+', 'match' ) );
					ystart = str2double( regexp( text, '(?<=ystart\=)\s*\d+\.\d+', 'match' ) );
					yend = str2double( regexp( text, '(?<=yend\=)\s*\d+\.\d+', 'match' ) );
				else
					warning("Missing %s file", [ "nest_", num2str(i), ".nml"] );
				endif
				
				cms_output.xstart = [ cms_output.xstart; xstart ];
				cms_output.xend = [ cms_output.xend; xend ];
				cms_output.ystart = [ cms_output.ystart; ystart ];
				cms_output.yend = [ cms_output.yend; yend ];
			endfor
		endif
		
	else
		warning("Input directory %s cannot be found\n", [ "../input_", cms_output.expt_name ] );
	endif
end