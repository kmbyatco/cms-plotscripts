% cluster_polygons script output variables:
%	label_xyz
%	labels
%	node_labels
% 	settle_pairs
%	filter_groups
%	filter_index
% 	settle
% 	settle_count
% 	ind_x
% 	ind_y
%	order
%	group_out
%	pagerank
%	group_names
%	cluster_id_old
%	cluster_sub_layer
%	group_out_min
%	group_out_max
% 	cluster_assignments
% 	xy_grouping
%	cluster_labels

%Generate node labels using release file points to have maximum descriptive locations
if( ( isfield(cms_output,"release_file") && ~isempty( cms_output.release_file ) ) )

	%Parse release points
	fid = fopen(cms_output.release_file);
	_release_xyz = textscan( fid, '%f %f %d', 'Delimiter', ' ');
	fclose(fid);
	label_xyz.lon = _release_xyz{1};
	label_xyz.lat = _release_xyz{2};
	label_xyz.poly = _release_xyz{3};

%else use node centers
else

	label_xyz.lon = xy_grouping(:,1);
	label_xyz.lat = xy_grouping(:,2);
	label_xyz.poly = xy_grouping(:,3);
	
endif 

%Generate labels for each release point
labels = shpLabels([label_xyz.lon,label_xyz.lat]);

%Create combined labels for each release point
node_labels = groupLabel( label_xyz.poly, 0, cluster_assignments, labels.municipality);

% convert the original polygon indices to the new indices after the regrouping step
settle_pairs = replaceValues( con_data(:,1:2), [1:length(cluster_assignments) ; cluster_assignments']' );

%remove empty groups due to spatial filtering
filter_groups = find(xy_grouping(:,1) == 0);
if( ~isempty(filter_groups) )
	filter_index = any( horzcat(	bsxfun( @eq, settle_pairs(:,1), filter_groups),
									bsxfun( @eq, settle_pairs(:,2), filter_groups))' );
	settle_pairs(filter_index,:) = [];
	xy_grouping(filter_groups,:)=[];
endif	

% set polygon indices as inputs to the matrix algorithm, with max_polygons to set extents of the matrix
[settle, settle_count] = createSettlementMatrix( settle_pairs, size(xy_grouping)(1) );

%find optimal grouping using infomap
[ind_x, ind_y] = ind2sub( size(settle), settle >= min(settle(settle>0)) );
[order, group_out, pagerank, group_names] = infomapCreateClusters( [ ind_x, ind_y, settle(settle>0) ], xy_grouping, node_labels );

%filtering subclusters
if( (cluster_id_old > 0) && ( size(group_out)(2) > 2) )

	% select sub cluster
	cluster_sub_layer = 2; % usually clusters groupings are only 3 layers deep
	group_out_min = find( group_out(:,1) == cluster_id_old, 1 );
	group_out_max = find( group_out(:,1) == cluster_id_old, 1, 'last');
	% groups = group_out(group_out_min:group_out_max, cluster_sub_layer);
	groups = [ zeros(group_out_min-1,1) ; group_out(group_out_min:group_out_max, cluster_sub_layer) ; zeros((size(xy_grouping)(1)-group_out_max),1) ] ;

% if no subcluters	
else
	if( (cluster_id_old > 0) )
		warning( "Clusters are only two layers deep, plotting all clusters" );
	endif
	% select everything
	groups = group_out(:,1); 
endif

% applying infomap corrections
cluster_assignments = replaceValues( cluster_assignments, [ order' ; 1:length(order) ]' );
xy_grouping=xy_grouping(order,:);
xy_grouping(:,3)=[1:size(xy_grouping)(1)];
settle_pairs = replaceValues( con_data(:,1:2), [1:length(cluster_assignments) ; cluster_assignments']' );
node_labels = node_labels(order);

%create node_label file
if( exist( sprintf("%s_labels.txt", cms_output.expt_name) ) )
	fid=fopen( sprintf("%s_labels.txt", cms_output.expt_name),'wt');
		for i=1:length(node_labels)
			fprintf(fid,'%s\n',node_labels{i})
		end
	fclose(fid);
endif

%remove empty groups due to spatial filtering
if( ~isempty(filter_groups) )
	filter_index = any( horzcat(	bsxfun( @eq, settle_pairs(:,1), filter_groups),
									bsxfun( @eq, settle_pairs(:,2), filter_groups))' );
	settle_pairs(filter_index,:) = [];
	cluster_assignments(cluster_assignments == filter_groups) = [];
endif

%remove NaN locations
settle_pairs( any(isnan(settle_pairs),2)', : ) = [];
xy_grouping( any(isnan(xy_grouping(:,1:2)))',:) = [];

%create correct node connections
[settle, settle_count] = createSettlementMatrix( settle_pairs, size(xy_grouping)(1) );

%Create combined labels for each cluster
cluster_labels = groupLabel( label_xyz.poly, groups, cluster_assignments, labels.municipality);