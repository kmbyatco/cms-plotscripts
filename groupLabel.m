% label polygons according to cluster id using grass
% release_polygons and labels must be the same in length and order

function cluster_labels = groupLabel( release_polygons, groups, cluster_assignments, labels )

	%Cluster string array
	cluster_labels = []; 

	%if plotting per cluster
	if( ~all(groups==0) )
	
		for i = 1 : max(groups)
			group_min = find( groups == i, 1);
			group_max = find( groups == i, 1, 'last');
			
			%Find polygon numbers clustered on group i, then search release for matching polygons via broadcasting
			index = any( bsxfun( @eq, release_polygons, find( cluster_assignments >= group_min & cluster_assignments <= group_max  )' )');
		
			%Match labels on index for group i
			group_label = labels(index);
			
			%Reduce to unique strings
			group_label = unique( group_label );
			
			%Combine unique strings from array to single long string
			single_label = []; 							%Current string
			single_label = char(single_label);
			for j = 1 : size(group_label)(1) 		%For each unique string
				single_label = [ single_label, group_label{j} ]; 	%Combine
				
				if ( j < size(group_label)(1) )		%if more than one unique, add comma
					single_label = [ single_label, "," ];
				endif
				
			endfor
			
			if( isempty(single_label) )
				single_label="Unknown";
			endif
			
			%Remove spaces to have less error in infomap parsing
			single_label(isspace(single_label)) = [];
			
			%Add to array
			cluster_labels{i}= single_label;
			
			%If there is a similar combined string already in the string array, append incremental number
			count = length(strmatch(single_label,cluster_labels));
			if( count > 1)
				cluster_labels{i} = [ cluster_labels{i}, sprintf("%d",count) ];
			endif
			
		endfor
	
	%if plotting per node
	else
	
		for i = 1 : max(cluster_assignments)
			
			% Find polygon numbers clustered on group i, then search release for matching polygons via broadcasting
			if( length(cluster_assignments) > length(release_polygons) ) %if not using release_file output
				index = release_polygons == i;
			else
				index = bsxfun( @eq, release_polygons, find( cluster_assignments == i )' )';
			endif
			
			%if using xy_grouping input, usually, release_polygons x cluster_assignments is a single vector
			
			if( size(index)(2) > 1 )
				index = any(index);
			endif
		
			% Match labels on index for group i
			group_label = labels(index);
			
			% Reduce to unique strings
			group_label = unique( group_label );
			
			% Combine unique strings from array to single long string
			single_label = []; 							%Current string
			single_label = char(single_label);
			for j = 1 : size(group_label)(1) 		%For each unique string
				single_label = [ single_label, group_label{j} ]; 	%Combine
				
				if ( j < size(group_label)(1) )		%if more than one unique, add comma
					single_label = [ single_label, "," ];
				endif	

				%limit of only three place names
				if( j ==3 )
					break;
				endif
			endfor
			
			if( isempty(single_label) )
				single_label="Unknown";
			endif
			
			%Remove spaces to have less error in infomap parsing
			single_label(isspace(single_label)) = [];
			
			% Add to array
			cluster_labels{i}= single_label;
			
			% If there is a similar combined string already in the string array, append incremental number
			count = length(strmatch(single_label,cluster_labels));
			if( count > 1)
				cluster_labels{i} = [ cluster_labels{i}, sprintf("%d",count) ];
			endif
			
		endfor
	endif
		
		
endfunction