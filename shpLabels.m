%This is a octave wrapper script to create labels from input points in reference to a pre-loaded Grass GIS database
function label = shpLabels( inputvectorarray )

	%Code can only be run on Octave as of now
	if ( isOctave() == 0 )
		error("Not running inside octave")
	endif

	function [province, municipality] = getLabel( inputvector, outstruct )
		
		[~,ind] = min( sqrt( sum( ([ outstruct.X; outstruct.Y ]' - inputvector)'.^2 ) ) );
		
		outstruct_index = 0;
		for i = 1:length(outstruct)
			outstruct_index += length(outstruct(i).X);
			if( outstruct_index > ind )
				break;
			endif
		endfor
		
		province = outstruct(i).NAME_1;
		municipality = outstruct(i).NAME_2;
	endfunction 
	
	pkg load mapping;
	
	temp_file=tempname();
	
	% parse data into input text file
	csvwrite( temp_file, inputvectorarray);
	
	% script output will generate a filename of the md5sum of the input file
	gsl_file = [ md5sum(temp_file), ".gsl"];
	
	if( ~exist(gsl_file) )
	
		outstruct = shaperead( [ dir_in_loadpath("cms-plotscripts") '/grassdata/shapefiles/MuniCities'] );
		
		fid = fopen( gsl_file, "w" );
		for j = 1:size( inputvectorarray )(1)
			[province, municipality] = getLabel( inputvectorarray(j,:), outstruct );
			fprintf( fid, "%s,%s\n", province, municipality  );
		endfor
		
		fclose( fid );
		
	endif
	
	% read output data 
	fid = fopen(gsl_file);
	temp_label = textscan( fid, '%s %s', 'Delimiter', ',');
	fclose(fid);
	label.province = temp_label{:,1};
	label.municipality = temp_label{:,2};
	
	% clean up
	system(["rm -f ", temp_file]);
	
endfunction