close all;

save_figures = 0;
plot_mmap = 1;

% Get experiment output at current directory
[~,expt_name,~] = fileparts( pwd() );
expt_name = expt_name(6:end);

switch( expt_name )
case "vip"
	remote_dir = '/basin/rmt/okhotsk/scratch/connectivity/rmtexpt/';
	if( ~exist("cms_output") || isempty( cms_output ) )
		cms_output = getCmsProperties( 	[remote_dir 'expt_vip_20160722/expt_vip'],
										[remote_dir 'expt_vip_20160723/expt_vip'] );
	endif
									
	cluster_distance = 4000;
	
case "davao"
	remote_dir = '/basin/rmt/okhotsk/scratch/connectivity/rmtexpt/';
	if( ~exist("cms_output") || isempty( cms_output ) )
		cms_output = getCmsProperties( 	[remote_dir 'expt_davao_20160615/expt_davao'],
										[remote_dir 'expt_davao_20160719/expt_davao'],
										[remote_dir 'expt_davao_20160720/expt_davao'] );
	endif
									
	if( ~exist("cluster_distance") || isempty( cluster_distance ) )
		cluster_distance = 8000;
	endif
	time_bounds = [10, 12];
	traj_data = 1;
	
	custom_locations = [];
	% cms_output.expt_name="Mati-Q1";
	% custom_locations = [ custom_locations; 126.263938889,6.90754444444 ; 126.258611111,6.92138611111 ];
	
	% cms_output.expt_name="Pujada-Q1";
	% custom_locations = [ custom_locations; 126.257419444,6.79430277778 ; 126.282497222,6.77388611111 ];
	
	% cms_output.expt_name="Mabini-Q1";
	% custom_locations = [ custom_locations; 125.845,7.29499722222 ; 125.840833333,7.29472222222 ];
	custom_locations = [ custom_locations; 125.845,7.29499722222];
	
	% cms_output.expt_name="Samal-Q1";
	custom_locations = [ custom_locations; 125.720277778,7.00305555556 ; 125.676413889,6.958275 ];

	% cms_output.expt_name="Digos-Q1";
	% custom_locations = [ custom_locations; 125.661191667,7.33125555556 ; 125.662363889,7.31840833333 ];
	custom_locations = [ custom_locations; 125.661191667,7.33125555556  ];

	highlight_nodes = {"SamalCity6" ; "SamalCity7"; "Mabini"; "SantaMaria2"};

	% cms_output.xstart = min(bounds)(1);
	% cms_output.xend = max(bounds)(1);
	% cms_output.ystart = min(bounds)(2);
	% cms_output.yend = max(bounds)(2);
	
	lon_bounds = [cms_output.xstart,cms_output.xend];
	lat_bounds = [cms_output.ystart,cms_output.yend];
	
case "southmindanao"
	
	if( ~exist("cms_output") || isempty( cms_output ) )
		cms_output = getCmsProperties( pwd() );
	endif
	
	cluster_distance = 40000;
	cms_output.xend = 127;
	cms_output.yend = 8;
	lon_bounds = [cms_output.xstart,cms_output.xend];
	lat_bounds = [cms_output.ystart,cms_output.yend];
	
case "largevip"

	if( ~exist("cms_output") || isempty( cms_output ) )
		cms_output = getCmsProperties( pwd() );
	endif
	
	cluster_distance = 40000;
	cms_output.release_file=[];
	cms_output.xstart = 119.4191220231892;
	cms_output.xend = 123.6139384154804;
	cms_output.ystart = 11.5467682553990;
	cms_output.yend = 14.3438479898978;
	
	lon_bounds = [cms_output.xstart,cms_output.xend];
	lat_bounds = [cms_output.ystart,cms_output.yend];
	
case "surigaodelnorte"
	cluster_distance = 10000;
	
case "northernpalawan"
	cluster_distance = 20000;
endswitch

%If there is spatial filters, get filtered data
if	( 	exist("lon_bounds") && ~isempty( lon_bounds ) && 
		exist("lat_bounds") && ~isempty( lat_bounds ) )
		
	if( ~exist("cms_output") || isempty( cms_output ) )
		cms_output = getCmsProperties( pwd() );
	endif
	
	% Parse each traj_file
	if( ~exist("traj_data") || isempty( traj_data ) )
		traj_data = readTrajOut(cms_output.traj_files, lon_bounds, lat_bounds);
	endif
	
	% Parse each con_file
	if( ~exist("con_data") || isempty( con_data ) ||
		~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) )
		con_data = readConOut(cms_output.con_files, time_bounds );
	endif
	
	% Cluster settlement polygons
	if( ~exist("xy_grouping") || isempty( xy_grouping ) ||
		~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) ||		
		~exist("cluster_distance_old") || isempty( cluster_distance_old ) )
		
		time_bounds_old = time_bounds; %must reset cluster_assignments at every time_bounds
		[xy_grouping, cluster_assignments,cluster_distance] = groupPolygons(cms_output.poly_file, cluster_distance,lon_bounds,lat_bounds,custom_locations);
		cluster_distance_old = cluster_distance;
	endif
endif

%sub cluster id
cluster_id = 0;

draw_node_overview;
draw_node_matrix;
draw_node_connections;