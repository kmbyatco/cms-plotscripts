%Script parameters
if( ~exist("save_figures") || isempty( save_figures ) )
	save_figures = 0;
endif

if( ~exist("cluster_distance") || isempty( cluster_distance ) )
	cluster_distance = 10000;
endif

if( ~exist("cluster_id") || isempty( cluster_id ) )
	cluster_id = 0;
endif

if( ~exist("plot_mmap") || isempty( plot_mmap ) )
	plot_mmap = 0;
endif

plot_size = [ 0, 0, 800, 800 ];
fontsize = 14;

%Code can only be run on Octave as of now
if ( isOctave() == 0 )
	error("Not running inside octave")
endif

%Close previous plot windows
if( exist("f1") )
	close( figure(f1) );
endif

%Get experiment output at current directory
cms_output = getCmsProperties( pwd() );

%Parse each traj_file
if( ~exist("traj_data") || isempty( traj_data ) )
	traj_data = readTrajOut(cms_output.traj_files);
endif

% plot particles proper
f1 = figure(1,'Position', plot_size );

if( plot_mmap )
	% Draw coastline using m_map
	f1 = drawCoast(f1, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize );
else
	%Draw coastline using input nest file
	f1 = drawCoast(f1, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize, cms_output.nest_files );
endif

%Get axis bounds for m_map checking
limits = xlim();

% Add municipal borders if using m_map
if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
	if( ~exist('Muni_shape') )
		Muni_shape=m_shaperead([ dir_in_loadpath("cms-plotscripts") '/grassdata/shapefiles/MuniCities']);
	endif
	for k=1:length(Muni_shape.ncst) 
		m_line(Muni_shape.ncst{k}(:,1),Muni_shape.ncst{k}(:,2)); 
	endfor
	printf("\n");
endif

hold on;

%Initialize loop variables
particle_graphics = []; % graphics handle for all particles
plot_count = 0;

%For each time entry inside traj_data
for plot_datetime = traj_data.datetime_range';
	plot_count++;

	%If saving output, skip rendering if current image already exists
	if( save_figures && exist( sprintf("plots/particletrack%04d.png", plot_count) )  )
		printf("Already plotted %d seconds since release, plots/particletrack%04d.png exist\n", plot_datetime, plot_count);
		fflush(stdout);
		continue;
	endif
	
	%Delete all particles on plot
	delete(particle_graphics);

	%Create a logical index for current time entry
	datetime_index = traj_data.datetime==plot_datetime;
	
	%Convert location into m_map coordinates if plotting with m_map, else use location directly
	if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
		[ x, y ] = m_ll2xy( traj_data.lon(datetime_index),traj_data.lat(datetime_index) );
	else
		x = traj_data.lon(datetime_index);
		y = traj_data.lat(datetime_index);
	endif
	
	%Plot to current figure
	particle_graphics = plot( x, y, '.' );
	
	total_particles = size(x)(1);
	title ( sprintf( "%06d particles %s", total_particles, ctime(plot_datetime+1262275200) ) ); %time index offset from UNIX epoch
	printf("Plotted %d particles on %s", total_particles, ctime(plot_datetime+1262275200) );
	fflush(stdout);
	
	drawnow;
	
	if( save_figures )
		system( "mkdir -p plots");
		print( sprintf("plots/particletrack%04d.png", plot_count), '-dpng', '-color', sprintf("-S%d,%d",plot_size(3),plot_size(4)) );
	else
		drawnow;
	endif
	
endfor

hold off;