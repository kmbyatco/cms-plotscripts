%Script parameters
if( ~exist("save_figures") || isempty( save_figures ) )
	save_figures = 0;
endif

if( ~exist("cluster_distance") || isempty( cluster_distance ) )
	cluster_distance = 10000;
endif

if( ~exist("cluster_id") || isempty( cluster_id ) )
	cluster_id = 0;
endif

if( ~exist("time_bounds") || isempty( time_bounds ) )
	time_bounds = [1, 12];
endif

if( ~exist("plot_mmap") || isempty( plot_mmap ) )
	plot_mmap = 0;
endif

plot_size = [ 0, 0, 800, 800 ];
fontsize = 14;

%Code can only be run on Octave as of now
if ( isOctave() == 0 )
	error("Not running inside octave")
endif

%Close previous plot windows
if( exist("f3") )
	close( figure(f3) );
endif

%Get experiment output at current directory
if( ~exist("cms_output") || isempty( cms_output ) )
	cms_output = getCmsProperties( pwd() );
endif

% Parse each traj_file
if( ~exist("traj_data") || isempty( traj_data ) )
	traj_data = readTrajOut(cms_output.traj_files);
endif

%Cluster settlement polygons

%Clear variables when new paramters are set
if( ~exist("cluster_distance_old") || isempty( cluster_distance_old ) )
	cluster_distance_old = cluster_distance;
	clear xy_grouping;
endif

if(	~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) )
	time_bounds_old = time_bounds; %must reset cluster_assignments at every time_bounds
	clear con_data;
	clear xy_grouping;
	clear cluster_assignments;
endif

% Parse each con_file
if( ~exist("con_data") || isempty( con_data ) )
	con_data = readConOut(cms_output.con_files, time_bounds );
endif
	
% Cluster settlement polygons
if( ~exist("xy_grouping") || isempty( xy_grouping ) )
	[xy_grouping, cluster_assignments] = groupPolygons(cms_output.poly_file, cluster_distance_old);
endif
		
if( ( ~exist("cluster_id_old") || (cluster_id_old ~= cluster_id) ) )
	cluster_id_old = cluster_id;
	
	% cluster_polygons script output variables:
	%	label_xyz
	%	labels
	%	node_labels
	% 	settle_pairs
	%	filter_groups
	%	filter_index
	% 	settle
	% 	settle_count
	% 	ind_x
	% 	ind_y
	%	order
	%	group_out
	%	pagerank
	%	group_names
	%	cluster_id_old
	%	cluster_sub_layer
	%	group_out_min
	%	group_out_max
	% 	cluster_assignments
	% 	xy_grouping
	%	cluster_labels
	cluster_polygons
endif

% plot node overview proper
f3 = figure(3,'Position', plot_size );

if( plot_mmap )
	% Draw coastline using m_map
	f3 = drawCoast(f3, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize );
else                                                                                              
	% Draw coastline using input nest file                                                        
	f3 = drawCoast(f3, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize, cms_output.nest_files );
endif

%Get axis bounds for m_map checking
limits = xlim();

hold on;

%% create track brackground from settled tracks
% remove dull tracks
plot_traj.lon = traj_data.lon(:, find( traj_data.exitcode(1,:)==-4 ) );
plot_traj.lat = traj_data.lat(:, find( traj_data.exitcode(1,:)==-4 ) );
plot_traj.datetime = traj_data.datetime( :, find( traj_data.exitcode(1,:)==-4 ) );

%measure each settlement matrix link
% get non-empty polygon nodes
[group_x_all, group_y_all] = ind2sub( size(settle), settle > 0 );

%generate edge list
group_range = unique( [group_x_all, group_y_all] );

%get all starting track locations
plot_traj.lon_starts=plot_traj.lon(1,:);
plot_traj.lat_starts=plot_traj.lat(1,:);
plot_traj.datetime_starts=plot_traj.datetime(1,:);

%get all ending track locations
isnan_matrix = bsxfun( @and, diff(isnan(plot_traj.lon)), diff(isnan(plot_traj.lat)) );
isnan_matrix( size(isnan_matrix)(1)+1, size(isnan_matrix)(2)+1 ) = 0; %diff reduces resuting vector size, this is to undo that
plot_traj.lon_ends=plot_traj.lon(logical(isnan_matrix))';
plot_traj.lat_ends=plot_traj.lat(logical(isnan_matrix))';
plot_traj.datetime_ends=plot_traj.datetime(logical(isnan_matrix))';

%start datetime filtering - get only releases starting from time_bounds
date_start=localtime(traj_data.datetime_range(1)+1262275200);
date_end=localtime(traj_data.datetime_range(end)+1262275200);
date_quarter = date_start;
plot_index_dates=zeros( size(plot_traj.lon_ends) );
for i = date_start.year:date_end.year %iterate for all years of run
	
	date_quarter.year = i;
	
	date_quarter.mon = min(time_bounds_old)-1;
	date_quarter.mday = 1;
	month_start=mktime(date_quarter)-1262275200;
	
	date_quarter.mon = max(time_bounds_old)-1;
	date_quarter.mday = eomday(date_quarter.year,date_quarter.mon);
	month_end=mktime(date_quarter)-1262275200;
	
	plot_index_date = all( [	bsxfun( @gt, plot_traj.datetime_starts, month_start ),
								bsxfun( @lt, plot_traj.datetime_ends, month_end ) ] );
								
	plot_index_dates = plot_index_dates | plot_index_date;
endfor

%draw each nodes tracks
for j = 1 : size(xy_grouping)(1)
	%get lon lat of source node j
	plot_node_start_lon = xy_grouping(j,1);
	plot_node_start_lat = xy_grouping(j,2);

	%find traj index of tracks starting at node j
	plot_index_start = all( [	bsxfun( @ge, plot_traj.lon_starts, plot_node_start_lon-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lon_starts, plot_node_start_lon+km2deg(cluster_distance/1000) );
								bsxfun( @ge, plot_traj.lat_starts, plot_node_start_lat-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lat_starts, plot_node_start_lat+km2deg(cluster_distance/1000) ); ] );

	%get lon lat of  all sink nodes of j
	plot_node_end_index = group_y_all(group_x_all == j); %get both sink and source, else try columns
	plot_node_end_lon = xy_grouping( plot_node_end_index, 1 ); %get locations from node list
	plot_node_end_lat = xy_grouping( plot_node_end_index, 2 );

	%find traj index of tracks ending at node k, k is all sinks nodes of node j
	plot_index_ends = zeros( size(plot_traj.lon_ends) );
	for k = 1 :length(plot_node_end_lon)
		plot_index_end = all( [	bsxfun( @ge, plot_traj.lon_ends, plot_node_end_lon(k)-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lon_ends, plot_node_end_lon(k)+km2deg(cluster_distance/1000) ); 
								bsxfun( @ge, plot_traj.lat_ends, plot_node_end_lat(k)-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lat_ends, plot_node_end_lat(k)+km2deg(cluster_distance/1000) ); ] );
								
		plot_index_ends = plot_index_ends | plot_index_end;
		
		% combine all indices to filter out desired tracks
	endfor
	sum_plot_k = sum( plot_index_start & plot_index_ends & plot_index_dates );
	
	%find traj index of tracks ending at node k, k is all sinks nodes of node j
	plot_index_ends = zeros( size(plot_traj.lon_ends) );
	for k = 1 :length(plot_node_end_lon)
		plot_index_end = all( [	bsxfun( @ge, plot_traj.lon_ends, plot_node_end_lon(k)-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lon_ends, plot_node_end_lon(k)+km2deg(cluster_distance/1000) ); 
								bsxfun( @ge, plot_traj.lat_ends, plot_node_end_lat(k)-km2deg(cluster_distance/1000) );
								bsxfun( @le, plot_traj.lat_ends, plot_node_end_lat(k)+km2deg(cluster_distance/1000) ); ] );
								
		% if( ~sum(plot_index_start & plot_index_end) )
			% [j, xy_grouping(k,3)]
		% endif
		plot_index_ends = plot_index_ends | plot_index_end;


		% combine all indices to filter out desired tracks
		plot_index = plot_index_start & plot_index_ends & plot_index_dates;
		
		plot_traj_i_lon = plot_traj.lon( :, find(plot_index) );
		plot_traj_i_lat = plot_traj.lat( :, find(plot_index) );		
		
		if( isempty(plot_traj_i_lon) || isempty(plot_traj_i_lon) )
			continue;	
		endif
		
		% totaltraj = floor(sum_plot_k*settle(j,xy_grouping(k,3))); % plot <10 tracks per each source-sink
		totaltraj = 10; % plot <10 tracks per each source-sink
		for i =  linspace(1, size(plot_traj_i_lon)(2), totaltraj );
		% for i =  1:size(plot_traj_i_lon)(2)
			
			i = floor(i);
			
			% convert coordinates if needed
			if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
				[ x, y ] = m_ll2xy( plot_traj_i_lon(:,i), plot_traj_i_lat(:,i) );
			else
				x = plot_traj_i_lon(:,i);
				y = plot_traj_i_lat(:,i);
			endif
			
			% plotting tracks proper
			x = x';
			y = y';
			
			col = plot_traj.datetime(:,i)';
			col( isnan(x) ) = [];
			col = double(col);
			col = (col-min(col))/(max(col)-min(col));
			
			y( isnan(x) ) = [];
			x( isnan(x) ) = [];
			
			z = zeros(size(x) );
			
			if( isempty(x) )
				continue;
			endif
			
			surface([x;x]',[y;y]',[z;z]',[col;col]',...
				'facecolor','none',...
				'edgecolor','interp',...
				'linewidth',1+1*8000/10000);
				
		endfor
	endfor
endfor

%reduce node name lengths for plotting
% node_plot_names = cellfun( @(x) x( isstrprop(x, 'upper') | isstrprop(x, 'digit') | x=="," ), group_names, 'un', 0 );
node_plot_names = cellfun( @(x) x( isstrprop(x, 'upper') | isstrprop(x, 'digit') ), group_names, 'un', 0 );

% plot cluster centers
marker_size = 35;
overview_color = 0.8 * [ 1, 1, 1 ];
for i = 1 : size(xy_grouping)(1)

	% redo xy_grouping points which are outside the figure
	if( cms_output.xstart > xy_grouping(i,1) )
		xy_grouping(i,1) = cms_output.xstart;
	elseif( cms_output.xend < xy_grouping(i,1) )
		xy_grouping(i,1) = cms_output.xend;
	endif	
	if( cms_output.ystart > xy_grouping(i,2) )
		xy_grouping(i,2) = cms_output.ystart;
	elseif( cms_output.yend < xy_grouping(i,2) )
		xy_grouping(i,2) = cms_output.yend;
	endif

	%convert coordinates if needed
	if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
		[ x, y ] = m_ll2xy( xy_grouping(i,1), xy_grouping(i,2) );
	else
		x = xy_grouping(i,1);
		y = xy_grouping(i,2);
	endif

	%plot cluster circles
	plot( x, y,	'marker', 'o', 
				'markersize', marker_size,
				'markeredgecolor', 'black',
				'markerfacecolor', [0.8 0.8 0.8] );		
			
	%plot cluster labels
	text( x, y, 1, sprintf( "%s", node_plot_names{i} ),	'color', setTextColor(overview_color), 
										'fontweight', 'bold', 
										'fontsize', 8,
										'horizontalalignment', 'center',
										'verticalalignment', 'middle' );	
endfor

title( "Node Overview Plot", 'fontweight','bold',"fontsize", fontsize);

hold off;

overview_spacing = 20;

colormap(parula(overview_spacing));

min_time = min(plot_traj.datetime_starts(find(plot_index_dates))(:))+1262275200; %use min to show earliest release and settlement
max_time = max(plot_traj.datetime_ends(find(plot_index_dates))(:))+1262275200;
overview_label_interval = linspace( min_time, max_time, floor(overview_spacing/4) );

overview_tick_label = [];
overview_tick_label = char(overview_tick_label);
for i = 1:floor(overview_spacing/4)
	% overview_tick_label = [ overview_tick_label; strftime( "%R %D", localtime(overview_label_interval(i))) ];
	overview_tick_label = [ overview_tick_label; strftime( "%D", localtime(overview_label_interval(i))) ];
endfor
overview_tick_spacing = linspace(0,1,floor(overview_spacing/4));

h = colorbar( 'XTickLabel', overview_tick_label, 
					'XTick', overview_tick_spacing, 
					'fontsize', fontsize,
					'southoutside' );
% title( h, "Time at Position");

if( save_figures )
	system( "mkdir -p plots");
	print( sprintf("plots/Overview_%s_%d.svg", cms_output.expt_name, cluster_id ), '-dsvg', '-color', sprintf("-S%d,%d",plot_size(3),plot_size(4)) );
else
	drawnow;
endif