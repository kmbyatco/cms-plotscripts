% replace all values of the first column of 'lookup' inside 'old' with the second column of lookup
% save output to new
function new = replaceValues( old, lookup )
	
	new = old;
	for i = 1:length(lookup)
		new(old == lookup(i,1)) = lookup(i,2);
	endfor
	
endfunction
