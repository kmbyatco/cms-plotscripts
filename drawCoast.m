% Draw a coastline using either mmap (by default), or CMS input grid using input bounds
function [f1, Muni_shape] = drawCoast( figurehandle, varargin )
	
	addpath( [ dir_in_loadpath("cms-plotscripts") '/m_map'] ); %mmap files
	pkg load netcdf;
	
	switch length(varargin)
		case 4
			nest_filename = [ varargin{4} ];
			fontsize = varargin{3};
			lat_extents = varargin{2};
			lon_extents = varargin{1};
		case 4
			fontsize = varargin{4};
			Muni_shape = varargin{3};
			lat_extents = varargin{2};
			lon_extents = varargin{1};
		case 3
			fontsize = 10;
			Muni_shape = varargin{3};
			lat_extents = varargin{2};
			lon_extents = varargin{1};
		case 2
		case 1
		otherwise
			error( "Too many inputs in function" )
			return;
	endswitch
	
	
	if( exist("nest_filename") )

		ncid = netcdf_open(nest_filename,'NC_NOWRITE');
		varidLon = netcdf_inqVarID(ncid,'Longitude');
		lonAxis = netcdf_getVar(ncid,varidLon);
		varidLat = netcdf_inqVarID(ncid,'Latitude');
		latAxis = netcdf_getVar(ncid,varidLat);
		varidU = netcdf_inqVarID(ncid,'zu');
		uvel = netcdf_getVar(ncid,varidU);
		netcdf_close(ncid);
		
		% mask land and water
		layer=1;
		mask=squeeze(uvel(:,:,layer,1));
		mask(mask<2^100)=0; %if water
		mask(mask==2^100)= 1; %if land
		
		f1=figure(figurehandle);
		set(gca,"fontsize", fontsize);
		set(gca,"fontsize", fontsize);
		xlim(lon_extents);
		ylim(lat_extents);
		
		
		% draw the land 
		hold on;
		
		% cont = contourc(lonAxis,latAxis,mask',[0.5 0.5]);
		% cont_index = find(cont(1,:) == 0.5);
		% for i = 1:length(cont_index)
		
			% cont_start = cont_index(i)+1;
			% if( i == length(cont_index))
				% cont_end = length(cont);
			% else
				% cont_end = cont_index(i+1)-1;
			% endif
		
			% patch(cont(1,cont_start:cont_end), cont(2,cont_start:cont_end), [0.8 0.8 0.8]);
		% endfor
		
		contourf(lonAxis,latAxis,mask',[0.5 0.5]);
	else
	
		f1=figure(figurehandle);
		m_proj('mercator','lon',lon_extents,'lat',lat_extents);
		m_grid('tickdir','out','box',('fancy'),'fontsize',fontsize);
			
		% draw the land
		coast_name = sprintf("lon%d-%dlat%d-%d.mat", lon_extents(1), lon_extents(2), lat_extents(1), lat_extents(2))
		if( exist( coast_name, 'file' ) ~= 2 )
		
			Muni_shape = m_shaperead([ dir_in_loadpath("cms-plotscripts") '/grassdata/shapefiles/MuniCities']);
			Muni_min = cellfun( @min, Muni_shape.ncst, 'UniformOutput', false );
			Muni_max = cellfun( @max, Muni_shape.ncst, 'UniformOutput', false );
			Muni_min_index = cellfun ( @(x)( all(x >= [min(lon_extents) min(lat_extents)] ) ), Muni_min, 'UniformOutput', false );
			Muni_max_index = cellfun ( @(x)( all(x <= [max(lon_extents) max(lat_extents)]) ), Muni_max, 'UniformOutput', false );
			
			% include any found coastlines
			Muni_index = find( any( [cell2mat(Muni_max_index), cell2mat(Muni_min_index)]' ) );
			
			Muni_area=[];
			%sort by size
			for i = Muni_index
				area = Muni_shape.ncst{i};
				Muni_area = [ Muni_area, polyarea( area(:,1),area(:,2) ) ];
			endfor
			[~, Muni_b]=sort(Muni_area', 'descend');
			
			%create variables for coast mat file
			ncst = [];
			for i = Muni_b'
				area = Muni_shape.ncst{Muni_index(i)};
				ncst = [ ncst ; [ area(:,1), area(:,2) ] ] ;
				
				if( i ~= Muni_b(end) )
					ncst = [ ncst ; [NaN, NaN] ];
				endif
			endfor
			
			%see m_map/private/mu_coast.m for algorithm
			k=[find(isnan(ncst(:,1)))];
			Area=zeros(length(k)-1,1);
			for i=1:length(k)-1,
			  x=ncst([k(i)+1:(k(i+1)-1) k(i)+1],1);
			  y=ncst([k(i)+1:(k(i+1)-1) k(i)+1],2);
			  nl=length(x);
			  Area(i)=sum( diff(x).*(y(1:nl-1)+y(2:nl))/2 );
			end;
			
			save( coast_name, "ncst", "k", "Area");
		
		endif
		m_usercoast(coast_name,'patch',[0.8 0.8 0.8]);		
	endif

	axis equal;
	hold off;
	
endfunction;