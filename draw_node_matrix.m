%Script parameters
if( ~exist("save_figures") || isempty( save_figures ) )
	save_figures = 0;
endif

if( ~exist("cluster_distance") || isempty( cluster_distance ) )
	cluster_distance = 10000;
endif

if( ~exist("cluster_id") || isempty( cluster_id ) )
	cluster_id = 0;
endif

if( ~exist("time_bounds") || isempty( time_bounds ) )
	time_bounds = [1, 12];
endif

plot_size = [ 0, 0, 1200, 800 ];
plot_isbubble = 1;
fontsize = 14;

%Code can only be run on Octave as of now
if ( isOctave() == 0 )
	error("Not running inside octave")
endif

%Close previous plot windows
if( exist("f2") )
	close( figure(f2) );
endif

%Get experiment output at current directory
if( ~exist("cms_output") || isempty( cms_output ) )
	cms_output = getCmsProperties( pwd() );
endif

%Cluster settlement polygons

%Clear variables when new paramters are set
if( ~exist("cluster_distance_old") || isempty( cluster_distance_old ) )
	cluster_distance_old = cluster_distance;
	clear xy_grouping;
endif

if(	~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) )
	time_bounds_old = time_bounds; %must reset cluster_assignments at every time_bounds
	clear con_data;
	clear xy_grouping;
	clear cluster_assignments;
endif

% Parse each con_file
if( ~exist("con_data") || isempty( con_data ) )
	con_data = readConOut(cms_output.con_files, time_bounds );
endif
	
% Cluster settlement polygons
if( ~exist("xy_grouping") || isempty( xy_grouping ) )
	[xy_grouping, cluster_assignments] = groupPolygons(cms_output.poly_file, cluster_distance_old);
endif
		
if( ( ~exist("cluster_id_old") || (cluster_id_old ~= cluster_id) ) )
	cluster_id_old = cluster_id;
	
	% cluster_polygons script output variables:
	%	label_xyz
	%	labels
	%	node_labels
	% 	settle_pairs
	%	filter_groups
	%	filter_index
	% 	settle
	% 	settle_count
	% 	ind_x
	% 	ind_y
	%	order
	%	group_out
	%	pagerank
	%	group_names
	%	cluster_id_old
	%	cluster_sub_layer
	%	group_out_min
	%	group_out_max
	% 	cluster_assignments
	% 	xy_grouping
	%	cluster_labels
	cluster_polygons
endif
	
% set polygon indices as inputs to the matrix algorithm, with max_polygons to set extents of the matrix
[settle, settle_count] = createSettlementMatrix( settle_pairs, size(xy_grouping)(1) );

% set grouping colormap
if( ~exist("group_colormap_old") || isempty(group_colormap_old) ||
	~exist("group_colormap") || isempty(group_colormap) ||
	~exist("old_size_colormap") || isempty(old_size_colormap) ||
	~isequal(size(xy_grouping)(1),old_size_colormap) ||
	~isequal(group_colormap_old,group_colormap) )

	% default group color map
	group_colormap = lines(size(xy_grouping)(1));
	group_colormap = hsv2rgb( bsxfun( @times, rgb2hsv( group_colormap ), [ 1 0.6 1] ) );
	
	old_size_colormap = size(xy_grouping)(1);
	group_colormap_old = group_colormap;
endif

% lighten color for matrix backgrounds
group_colormap_matrix = hsv2rgb( bsxfun( @times, rgb2hsv( group_colormap ), [ 1 0.5 1] ) );

% Prepare plotting grid

%reduce node name lengths for plotting
node_plot_names = cellfun( @(x) x( isstrprop(x, 'upper') | isstrprop(x, 'digit') ), group_names, 'un', 0 );


if( exist("highlight_nodes") && ~isempty(highlight_nodes))

	settle_plot_index = zeros(size(settle));
	for i = 1:length(highlight_nodes)
		settle_plot_index( :, strmatch(highlight_nodes{i},group_names, 'exact' ) ) = 1;
		settle_plot_index( strmatch(highlight_nodes{i},group_names, 'exact' ), : ) = 1;
	endfor
	
	settle_plot = settle .* settle_plot_index;
	
else	
	settle_plot=settle; %create copy of settlement matrix for plotting
endif

% plot settlement matrix proper
f2 = figure(2,'Position', plot_size );

hold on;

% create diagonals for self-seeding
settle_size = size(xy_grouping)(1); 
plot([1:settle_size],[1:settle_size],'linestyle','-','color','black');

% create group ticks and rectangles
for i = 1 : max(group_out(:,1))
	group_min = find( group_out(:,1) == i, 1 );
	group_max = find( group_out(:,1) == i, 1, 'last');
	
	if( isempty(group_min) && isempty(group_max) )
		continue;
	endif
	
	rectangle("Position",[group_min,group_min,group_max-group_min,group_max-group_min], 'FaceColor', [0.9,0.9,0.9], 'LineStyle', 'none');
	
endfor

% create per node ticks
if( exist("highlight_nodes") && ~isempty(highlight_nodes))
	ytick_spacing=[];
	for i = 1:length(highlight_nodes)
		ytick_spacing = [ ytick_spacing, strmatch(highlight_nodes{i},group_names, 'exact') ];
	endfor
else
	ytick_spacing = 1 : size(xy_grouping)(1);	
endif

ytick_labels = [];
if( exist("highlight_nodes") && ~isempty(highlight_nodes))
	for i = 1:length(highlight_nodes)
		yname = group_names{ strmatch(highlight_nodes{i},group_names, 'exact') };
		if( length(yname) > 10 ) 
			ytick_labels{end+1} = [ yname(1:10) ".." ];
		else
			ytick_labels{end+1} = yname;
		endif
	endfor
else
	for i = 1 : size(xy_grouping)(1)			
		yname = group_names{i};	
		if( length(yname) > 10 ) 
			ytick_labels{end+1} = [ yname(1:10) ".." ];
		else
			ytick_labels{end+1} = yname;
		endif
	endfor
endif

for i = 1 : size(xy_grouping)(1)			
	rectangle("Position",[group_min,group_min,group_max-group_min,group_max-group_min], 'FaceColor', group_colormap_matrix(i,:), 'LineStyle', 'none');
endfor

% create sub cluster group ticks and rectangles

if( exist("highlight_nodes") && ~isempty(highlight_nodes))
	xtick_spacing=[];
	for i = 1:length(highlight_nodes)
		xtick_spacing = [ xtick_spacing, strmatch(highlight_nodes{i},group_names, 'exact') ];
	endfor
else
	xtick_spacing = [];	
endif

xtick_labels =  [];
if( exist("highlight_nodes") && ~isempty(highlight_nodes))
	for i = 1:length(highlight_nodes)
			xtick_labels{end+1} = node_plot_names(strmatch(highlight_nodes{i},group_names, 'exact'));
	endfor
else
	for i = 1 : max(groups)
		group_min = find( groups == i, 1 );
		group_max = find( groups == i, 1, 'last');
		
		if( group_max-group_min > 10 )
			xname = group_names{group_min};
			if( length(xname) > 10 ) 
				xtick_labels{end+1} = [ xname(1:10) ".." ];
			else
				xtick_labels{end+1} = xname;
			endif
			xtick_labels{end+1} = "";
		else
			xtick_labels{end+1} = node_plot_names(group_min);
			xtick_labels{end+1} = "";
		endif
	endfor
endif

for i = 1 : max(groups)
	group_min = find( groups == i, 1 );
	group_max = find( groups == i, 1, 'last');

	if( isempty(group_min) && isempty(group_max) )
		continue;
	endif

	if( ~exist("highlight_nodes") || isempty(highlight_nodes))
		xtick_spacing = [ xtick_spacing, group_min, group_max ];
	endif
	
	rectangle("Position",[group_min,group_min,group_max-group_min,group_max-group_min], 'FaceColor', group_colormap_matrix(i,:), 'LineStyle', 'none');
endfor

set(gca,'XTick',xtick_spacing,"fontsize", fontsize);
set(gca,'YTick',ytick_spacing,"fontsize", fontsize);
set(gca,'Xticklabel',xtick_labels,"fontsize", fontsize);
set(gca,'Yticklabel',ytick_labels,"fontsize", fontsize);

% decide to plot in bubble or pcolor form
if( plot_isbubble )

	if( exist("highlight_nodes") && ~isempty(highlight_nodes))
		%define matrix size
		settle_size = size(xy_grouping)(1); 
		[x,y]=meshgrid(1:1:settle_size, 1:1:settle_size); 
		settle_plot2 = settle;
	
		%remove unplottable points
		x( settle_plot2 == 0 ) = [];
		y( settle_plot2 == 0 ) = [];
		settle_plot2( settle_plot2 == 0 ) = [];
		
		s=10; % if dense
		% s=20; % if sparse
		h = scatter(x(:),y(:), s, [0.8 0.8 0.8] , 'filled' );
	endif

	%define matrix size
	settle_size = size(xy_grouping)(1); 
	[x,y]=meshgrid(1:1:settle_size, 1:1:settle_size);
	
	%remove unplottable points
	x( settle_plot == 0 ) = [];
	y( settle_plot == 0 ) = [];
	settle_plot( settle_plot == 0 ) = [];
	
	s=10; % if dense
	% s=20; % if sparse
	h = scatter(x(:),y(:), s, settle_plot(:), 'filled' );
	
else
	%remove empty points
	settle_plot( settle_plot == 0 ) = NaN;

	h = pcolor(x, y, settle_plot ); 
	shading flat
endif

% set figure parameters
axis square;
grid on;
box on;
colormap(parula(7));
colorbar("fontsize", fontsize);
xlim([0,settle_size+1]);
ylim([0,settle_size+1]);
xlabel("Sink node", "fontsize", fontsize)
ylabel("Source node", "fontsize", fontsize)

title( "Percent Settlement Plot", 'fontweight','bold',"fontsize", fontsize);

hold off

if( save_figures )
	system( "mkdir -p plots");
	%plottable only in png due to bug with color bar
	print( sprintf("plots/Matrix_%s_%d.png", cms_output.expt_name, cluster_id), '-dpng', '-color', sprintf("-S%d,%d",plot_size(3),plot_size(4)), '-r600' );
else
	drawnow;
endif