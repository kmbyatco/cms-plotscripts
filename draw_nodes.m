pkg load netcdf
pkg load statistics
graphics_toolkit fltk 

%% REQUIRED FUNCTIONS
addpath( "/home/kevin/code/matlab" ); %read_grass_database(), groupXY.m functions
addpath( "/home/kevin/code/matlab/m_map" ); %mmap files
addpath( "/home/kevin/code/matlab/m_map/private" ); %mmap files

%% RUN TIME PARAMETERS

% run time flags
printf( "\nCurrent polygon file: %s\n", cat_filename);
fflush(stdout);

if( ~exist('isbatch_plot') )
	isbatch_plot = 0;
endif
if( isbatch_plot )
	% node_savefigure = 0;
	node_labelgroup = 1;
	if( exist('ncdata') & exist('_con_file') )
		node_skipread = 1;
	else
		node_skipread = 0;
	endif
else
	% ask to save figures
	node_savefigure = input("Save figures (1/0)? ")
	node_labelgroup = input("Label nodes (1/0)? ");
	% ask if variables need to be re-read
	if( exist('ncdata') & exist('_con_file') )
		node_skipread =  input("Skip Reading variables (1/0)? ")
	else
		node_skipread = 0;
	endif
endif

%% READ IN THE TRAJECTORY DATA
if( ~node_skipread )
	% traj_filenames=ls([ traj_dirname, repmat('output/traj_*', size(traj_dirname)(1), 1) ]) ;
	traj_filenames = [];
	for i = 1 : size(traj_dirname)(1)
		traj_filename = [ traj_dirname, repmat('output/traj_*', size(traj_dirname)(1),1)](i,:);
		traj_filenames = [ traj_filenames; ls(traj_filename)];
	endfor
	% traj_dirname='./';
	% traj_filenames = [repmat(traj_dirname, size(traj_filename)(1), 1 ), traj_filename];
	ncdata = read_cms_out(traj_filenames);
	
	con_filename=ls([ con_dirname, 'output/con_file_*'] );
	_con_file = read_con_out( con_filename, con_dirname );
	node_skipread = 1;
endif

%% RECLASSIFY POLYGON DATA
% read spatially coordinated GIS data from file
[ grass_philmuni, grass_polyfile ] = read_grass_database( cat_filename );  

%group data based from polygon file and category file( grass_polyfile, distance)
if( exist( 'custom_grouping' ) )
	[grouping, xyg, maxgrouping] = group_polygons( poly_filename, grass_polyfile, 'custom', custom_grouping, maxpolygons );
else
	if( exist( 'cdistance' ) )
			[grouping, xyg, maxgrouping] = group_polygons( poly_filename, grass_polyfile, 'distance', cdistance, maxpolygons );
		else
			[grouping, xyg, maxgrouping] = group_polygons( poly_filename, grass_polyfile, 'municipalities' );
		endif
endif

if( exist('custom_order' ) )
	grouping(:,3) = replace_values( grouping(:,3), custom_order );
	xyg(:,3) = replace_values( xyg(:,3), custom_order );
endif


if( node_labelgroup )
	grouping_label = label_groups( grass_philmuni, grouping, maxgrouping );
endif

node_label = cellfun( @(x) x( isstrprop(x, 'upper') | isstrprop(x, 'digit') | isstrprop(x, 'punct') ), grouping_label, 'un', 0 );
% node_label = grouping_label;

%% PLOTTING NODES

%% DRAW COAST
f5 = draw_coast(5, lon_extents, lat_extents);
hold on;
m_proj('mercator','lon',lon_extents,'lat',lat_extents); %Davao

figure(f5);
if( ~exist('Muni_shape') )
	Muni_shape=m_shaperead('/home/kevin/shp/MuniCities');
endif
% for k=unique(grouping(:,1))' 
for k=1:length(Muni_shape.ncst) 
	m_line(Muni_shape.ncst{k}(:,1),Muni_shape.ncst{k}(:,2)); 
end; 

% create track brackground from settled tracks
traj_lon = ncdata.lon(:, find( ncdata.exitcode(1,:)==-4 ) );
traj_lat = ncdata.lat(:, find( ncdata.exitcode(1,:)==-4 ) );
traj_datetime = ncdata.datetime( :, find( ncdata.exitcode(1,:)==-4 ) );

traj_lon = traj_lon( :, find( traj_datetime(1,:) >= traj_datetimestart ) );
traj_lat = traj_lat( :, find( traj_datetime(1,:) >= traj_datetimestart ) );
traj_datetime = traj_datetime( :, find( traj_datetime(1,:) >= traj_datetimestart ) );

traj_lon = traj_lon( :, find( traj_datetime(1,:) <= traj_datetimeend ) );
traj_lat = traj_lat( :, find( traj_datetime(1,:) <= traj_datetimeend ) );

totaltraj = 500; %plot only 500 tracks
for i =  linspace(1, size(traj_lon)(2), totaltraj );
	i = floor(i);
	[ m_llx, m_lly ] = m_ll2xy( traj_lon(:,i), traj_lat(:,i) );
	% h = plot( m_llx, m_lly, ':', 'color', 'k', 'linewidth', 1+1*cdistance/10000 );
	h = plot( m_llx, m_lly, ':', 'color', 'k', 'linewidth', 1+1*8000/10000 );
	set (h, 'color', [ 255, 153, 51 ]/255 );
	if( ~mod(i,100) )
		printf(" plotting track %d of %d\n", i, size(traj_lon)(2) );
		fflush(stdout);
		drawnow;
	endif
endfor

% redo xyg points which are outside the figure
for i = 1 : length(xyg)
	if( min(lon_extents) > xyg(i,1) )
		xyg(i,1) = min(lon_extents);
	elseif( max(lon_extents) < xyg(i,1) )
		xyg(i,1) = max(lon_extents);
	endif
	
	if( min(lat_extents) > xyg(i,2) )
		xyg(i,2) = min(lat_extents);
	elseif( max(lat_extents) < xyg(i,2) )
		xyg(i,2) = max(lat_extents);
	endif
endfor

for i = 1 : length(xyg)

	if( ~ismember(i, whitelist) )
		continue;
	endif

	[ m_llx, m_lly ] = m_ll2xy(  xyg(i,1),  xyg(i,2) );
	% h = plot( m_llx, m_lly, '.', 'color', 'c' );
	h = plot( m_llx, m_lly, 'color', [0, 195, 255]/255 );
	% set (h, 'markersize', 30+8*cdistance/10000);
	set (h, 'markersize', 30+8*8000/10000);
endfor

for i = 1 : length(xyg)

	if( ~ismember(i, whitelist) )
		continue;
	endif

	[ m_llx, m_lly ] = m_ll2xy(  xyg(i,1),  xyg(i,2) );
	h = text( m_llx, m_lly, sprintf("%d", i), 'color', 'black', 'fontweight', 'bold', 'fontsize', 10 );
	% h = text( m_llx, m_lly, sprintf("%d %s", i, grouping_label{i}), 'color', 'black', 'fontweight', 'bold', 'fontsize', 10 );
	
	set (h, 'horizontalalignment', 'center' );
	if( exist('node_customalign') )
		if( i <= length(node_customalign) )
			if( ~isempty(node_customalign{i}) )
				set (h, 'horizontalalignment', node_customalign{i} );
			endif
		endif
	endif
endfor

if( node_resize )
	figure(f5, 'position', node_size)
endif

title( nodelist_plottitle, 'fontsize', 15, 'fontweight','b','color','k' );
% fontsize=15;
% set(gca, 'fontsize', fontsize );

if( node_savefigure )
	system( "mkdir -p plots");
	% figure( f5, 'Position',plotsize);
	% saveas( f5, sprintf( nodelist_plotloc ) );
	print( nodelist_plotloc, '-dsvg', '-color', sprintf("-S%d,%d",node_size(3),node_size(4)) );
else
	drawnow;
endif


hold off;