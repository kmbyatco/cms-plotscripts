%This is a octave wrapper script to create
function [order, groups, pagerank, names] = infomapCreateClusters( inputvector, xy_grouping, node_labels )

	%Code can only be run on Octave as of now
	if ( isOctave() == 0 )
		error("Not running inside octave")
	endif

	temp_file=tempname();
		
	fid = fopen(temp_file, 'w');
	fprintf( fid, "*Vertices %d\n", length(xy_grouping)(1) );
	for i = 1:length(xy_grouping)(1)
		fprintf( fid, "%d \"%s\"\n", i, node_labels{i} );
	endfor
	fprintf( fid, "*Edges %d\n", length(inputvector)(1) );
	fclose(fid);
	
	% parse data into input text file
	dlmwrite( temp_file, inputvector, ' ', "-append");
	% system( ["cat " temp_file] )
	
	% script output will generate a filename of the md5sum of the input file
	tree_file = [ md5sum(temp_file), ".tree"];
	
	if( exist(tree_file) ~= 2 )
		% generate labels using infomap
		system(["/opt/infomap/Infomap -i pajek ", temp_file, " ./ -d --tree --out-name ", md5sum(temp_file)]);		
	endif

	% read output data 
	fid = fopen( tree_file );
	tree_out = textscan( fid,'%s %f %s %d', 'Delimiter', ' ', 'HeaderLines',2);
	fclose(fid);
	pagerank = tree_out{2};
	names = tree_out{3};
	order = tree_out{4};

	names = cellfun( @(x) x( x~='"' ), names, 'un', 0 );
	
	%split data between colons
	group_out = regexp(tree_out{1},':', 'split');
	
	%parse module clustering
	groups = zeros( length(group_out),length(group_out{1}) );
	for i = 1:length(group_out)
		for j = 1:length(group_out{i})
			groups(i,j) = str2num(group_out{i}{j});
		endfor
	endfor
		
	% clean up
	system(["rm -f ", temp_file]);
endfunction
