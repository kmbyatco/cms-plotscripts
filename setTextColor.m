%set text color when given a colormap
function text_color = setTextColor( color )

	%Code can only be run on Octave as of now
	if ( isOctave() == 0 )
		error("Not running inside octave")
	endif	

	if( ~exist("color") || isempty(color) || length(color) ~= 3 )
		error("invalid input")
	endif

	
	if ( (1-sum( color .* [0.299, 0.587, 0.114 ])) < 0.5)
		text_color = [ 0, 0, 0 ]; % bright colors - black font
	else
		text_color = [ 1, 1, 1 ]; % dark colors - white font
	endif
	
endfunction
