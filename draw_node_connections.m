%Script parameters
if( ~exist("save_figures") || isempty( save_figures ) )
	save_figures = 0;
endif

if( ~exist("cluster_distance") || isempty( cluster_distance ) )
	cluster_distance = 10000;
endif

if( ~exist("cluster_id") || isempty( cluster_id ) )
	cluster_id = 0;
endif

if( ~exist("time_bounds") || isempty( time_bounds ) )
	time_bounds = [1, 12];
endif

if( ~exist("plot_mmap") || isempty( plot_mmap ) )
	plot_mmap = 0;
endif

plot_size = [ 0, 0, 800, 800 ];
fontsize = 14;

%Code can only be run on Octave as of now
if ( isOctave() == 0 )
	error("Not running inside octave")
endif

%Close previous plot windows
if( exist("f4") )
	close( figure(f4) );
endif

%Get experiment output at current directory
if( ~exist("cms_output") || isempty( cms_output ) )
	cms_output = getCmsProperties( pwd() );
endif

% Parse each traj_file
if( ~exist("traj_data") || isempty( traj_data ) )
	traj_data = readTrajOut(cms_output.traj_files);
endif

%Cluster settlement polygons
if( ( ~exist("cluster_distance_old") || (cluster_distance_old ~= cluster_distance) ) ||
	( ~exist("cluster_id_old") || (cluster_id_old ~= cluster_id) ) ||
	( ~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) ) ||
	( ~exist("con_data") || isempty( con_data ) ) ||
	( ~exist("cluster_assignments") || isempty( cluster_assignments ) ) ||
	( ~exist("xy_grouping") || isempty( xy_grouping ) )  ||
	( ~exist("labels") || isempty( labels ) ) ||
	( ~exist("settle") || isempty( settle ) ) )
	
	cluster_id_old = cluster_id;
	
	% Parse each con_file
	if( ~exist("con_data") || isempty( con_data ) ||
		~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) )
		con_data = readConOut(cms_output.con_files, time_bounds );
	endif
	
	% Cluster settlement polygons
	if( ~exist("xy_grouping") || isempty( xy_grouping ) ||
		~exist("time_bounds_old") || any(time_bounds_old ~= time_bounds) ||
		~exist("cluster_distance_old") || isempty( cluster_distance_old ) )
		
		cluster_distance_old = cluster_distance;
		time_bounds_old = time_bounds; %must reset cluster_assignments at every time_bounds
		[xy_grouping, cluster_assignments] = groupPolygons(cms_output.poly_file, cluster_distance_old);
	endif
			
	% cluster_polygons script output variables:
	%	label_xyz
	%	labels
	%	node_labels
	% 	settle_pairs
	%	filter_groups
	%	filter_index
	% 	settle
	% 	settle_count
	% 	ind_x
	% 	ind_y
	%	order
	%	group_out
	%	pagerank
	%	group_names
	%	cluster_id_old
	%	cluster_sub_layer
	%	group_out_min
	%	group_out_max
	% 	cluster_assignments
	% 	xy_grouping
	%	cluster_labels
	cluster_polygons
endif

% plot node connections proper
f4 = figure(4,'Position', plot_size );

if( plot_mmap )
	% Draw coastline using m_map
	f4 = drawCoast(f4, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize );
else                                                                                                       
	% Draw coastline using input nest file                                                                 
	f4 = drawCoast(f4, [cms_output.xstart, cms_output.xend], [cms_output.ystart, cms_output.yend], fontsize,  cms_output.nest_files );
endif

%Get axis bounds for m_map checking
limits = xlim();

hold on;

%Plot all edges first

% get non-empty polygon nodes
[group_x_all, group_y_all] = ind2sub( size(settle), settle > 0 );

%generate edge list
group_range = unique( [group_x_all, group_y_all] );

% each group's node centers
group_lon = [];
group_lat = [];
for i = 1 : length(group_range)
	group_lon = [ group_lon; xy_grouping( xy_grouping(:,3) == group_range(i),1 ) ];
	group_lat = [ group_lat; xy_grouping( xy_grouping(:,3) == group_range(i),2 ) ];
endfor

% connect nodes with using their edges
line_width = 1+1*8000/10000;
for i = 1 : length(group_x_all)

	% if self-seeding, draw no line
	if (group_x_all(i) == group_y_all(i))
		continue;
	endif
	
	%select ith entry from edge list
	j=[group_x_all, group_y_all](i,:);
	
	% convert coordinates if needed
	if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
		[ x, y ] = m_ll2xy(  group_lon(max(bsxfun( @eq, group_range,j )')), group_lat(max(bsxfun( @eq, group_range,j )')) );
	else
		x = group_lon(max(bsxfun( @eq, group_range,j )'));
		y = group_lat(max(bsxfun( @eq, group_range,j )'));
	endif
	
	% plot edges
	h = plot( x, y, 'color', 'blue' );
	set (h, 'linewidth', line_width, 'linestyle', '-', 'color', [0.8 0.8 0.8] );

endfor

% set grouping colormap
if( ~exist("group_colormap_old") || isempty(group_colormap_old) ||
	~exist("group_colormap") || isempty(group_colormap) ||
	~exist("old_size_colormap") || isempty(old_size_colormap) ||
	~isequal(size(xy_grouping)(1),old_size_colormap) ||
	~isequal(group_colormap_old,group_colormap) )

	% default group color map
	group_colormap = lines(size(xy_grouping)(1));
	group_colormap = hsv2rgb( bsxfun( @times, rgb2hsv( group_colormap ), [ 1 0.6 1] ) );
	
	old_size_colormap = size(xy_grouping)(1);
	group_colormap_old = group_colormap;
endif

%reduce node name lengths for plotting
node_plot_names = cellfun( @(x) x( isstrprop(x, 'upper') | isstrprop(x, 'digit') ), group_names, 'un', 0 );

if( exist("highlight_nodes") && ~isempty(highlight_nodes))
	highlight_node_plot=[];
	for i = 1:length(highlight_nodes)
		highlight_node_plot = [ highlight_node_plot, strmatch(highlight_nodes{i},group_names, 'exact') ];
	endfor
endif

% start per group plotting loop
marker_size = 35;
for group_i = 0:max(groups)
	
	% generate grouping index
	group_min = find( groups == group_i, 1);
	group_max = find( groups == group_i, 1, 'last');
	
	if( isempty(group_min) && isempty(group_max) )
		continue;
	endif
	
	group_index = (group_x_all >= group_min & group_x_all <= group_max & group_y_all >= group_min & group_y_all <= group_max );
	
	% generate edge list
	group_range = [ group_min : group_max ]';
	
	% filter out other groups
	source = group_x_all(group_index);
	sink = group_y_all(group_index);
	
	% set group color
	if ( group_i == 0 )
		group_color = [0.6 0.6 0.6];
	else
		group_color = group_colormap(group_i,:);
	endif
		
	% each group's node centers
	group_lon = [];
	group_lat = [];
	for i = 1 : length(group_range)
		group_lon = [ group_lon; xy_grouping( xy_grouping(:,3) == group_range(i),1 ) ];
		group_lat = [ group_lat; xy_grouping( xy_grouping(:,3) == group_range(i),2 ) ];
	endfor
	
	% plot grouped node's internal edges
	for i = 1 : length(source)

		% if plotting group 0, skip
		if ( group_i == 0 )
			continue;
		endif
	
		% if self-seeding, draw no line
		if (source(i) == sink(i))
			continue;
		endif
		
		j=[source, sink](i,:);
		
		% convert coordinates if needed
		if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
			[ x, y ] = m_ll2xy(  group_lon(max(bsxfun( @eq, group_range,j )')), group_lat(max(bsxfun( @eq, group_range,j )')) );
		else
			x = group_lon(max(bsxfun( @eq, group_range,j )'));
			y = group_lat(max(bsxfun( @eq, group_range,j )'));
		endif
	
		if( exist("highlight_nodes") && ~isempty(highlight_nodes)  )
			if( any(ismember(highlight_node_plot,j)) )
				%plot edges
				h = plot( x, y, 'color', 'blue' );
				set (h, 'linewidth', line_width, 'linestyle', '-', 'color', group_color );
			else
				h = plot( x, y, 'color', 'blue' );
				set (h, 'linewidth', line_width, 'linestyle', '-', 'color', [0.8 0.8 0.8] );
			endif
		else
			%plot edges
			h = plot( x, y, 'color', 'blue' );
			set (h, 'linewidth', line_width, 'linestyle', '-', 'color', group_color );
		endif
	endfor

	% plot node circles with color
	group_color_orig = group_color;
	for i = 1 : length(group_range)
	
		if( exist("highlight_nodes") && ~isempty(highlight_nodes)  )
			if( any(ismember(highlight_node_plot,group_range(i))) )
				group_color = group_color_orig;
			elseif( any(ismember(highlight_node_plot,group_range)) )
				group_color = hsv2rgb( bsxfun( @times, rgb2hsv( group_color_orig ), [ 1 0.8 1] ));
			else
				group_color = [0.8 0.8 0.8];
			endif
		endif
		
		type = sum( [source, sink] == group_range(i), 1);
		
		%convert coordinates if needed
		if( limits(2) <= cms_output.xstart ) % m_map has its own axis scaling
			[ x, y ] =  m_ll2xy(  group_lon(i), group_lat(i) );
		else
			x = group_lon(i);
			y = group_lat(i);
		endif
		
		% plot cluster centers
		
		% if source
		if( ( (type(1)+type(2)) > 0 ) &&
			( type(1)/(type(1)+type(2)) > 0.7) )
			
			h = plot( x, y,	'marker', '^', 
						'markersize', marker_size,
						'markeredgecolor', 'black',
						'markerfacecolor', group_color);
					
			text( x, y, 1, sprintf("%s", node_plot_names{group_range(i)}),	
															'color', setTextColor(group_color), 
															'fontweight', 'bold', 
															'fontsize', 10,
															'horizontalalignment', 'center',
															'verticalalignment', 'cap' );
			
		% if sink
		elseif( ( (type(1)+type(2)) > 0 ) && 
			(type(2)/(type(1)+type(2)) > 0.7 ) )
						
			h = plot( x, y,	'marker', 'v', 
						'markersize', marker_size,
						'markeredgecolor', 'black',
						'markerfacecolor', group_color);
					
			text( x, y, 1, sprintf("%s", node_plot_names{group_range(i)}),	
															'color', setTextColor(group_color), 
															'fontweight', 'bold', 
															'fontsize', 10,
															'horizontalalignment', 'center',
															'verticalalignment', 'bottom' );

		% if zero
		elseif( sum(type(1)+type(2)) == 0 )
		
			h = plot( x, y,	'marker', 'o', 
						'markersize', marker_size,
						'markeredgecolor', 'black',
						'markerfacecolor', [0.6 0.6 0.6] );		
			text( x, y, 1, sprintf("%s", node_plot_names{group_range(i)}),	
															'color', setTextColor([0.6 0.6 0.6]),
															'fontweight', 'bold', 
															'fontsize', 10,
															'horizontalalignment', 'center',
															'verticalalignment', 'middle' );
															
		else	
		% if self-seeding
			
			h = plot( x, y,	'marker', 'o', 
							'markersize', marker_size,
							'markeredgecolor', 'black',
							'markerfacecolor', group_color );	
					
			text( x, y, 1, sprintf("%s", node_plot_names{group_range(i)}),	
															'color', setTextColor(group_color), 
															'fontweight', 'bold', 
															'fontsize', 10,
															'horizontalalignment', 'center',
															'verticalalignment', 'middle' );
		endif
		
		if( exist("highlight_nodes") && ~isempty(highlight_nodes)  )
			if( any(ismember(highlight_node_plot,group_range(i))) )
				set (h, 'markeredgecolor', 'cyan' );
			endif
		endif
		
	endfor
		
	
endfor 

title( "Node Connections Plot", 'fontweight','bold',"fontsize", fontsize);

% legend({"Source","Sink","Self-Seeding","Empty"},'Location','southoutside','orientation','horizontal')

hold off;

if( save_figures )
	system( "mkdir -p plots");
	print( sprintf("plots/Connections_%s_%d.svg", cms_output.expt_name, cluster_id), '-dsvg', '-color', sprintf("-S%d,%d",plot_size(3),plot_size(4)) );
else
	drawnow;
endif