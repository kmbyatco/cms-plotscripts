#!/bin/bash

#Create temporary in-script input filename
temp_file=$(mktemp)

#Get input from script argument
cat $1 > $temp_file.csv

#Create output file name
out_file=$(md5sum $temp_file.csv | awk '{ print $1 }').gsl

if [[ -s $out_file ]]; then
	# cat $out_file;
	exit;
fi

#This script requires GrassGIS commands, invoked using modules
eval `tclsh /opt/tcl/modulecmd.tcl sh autoinit`
module load grass

if [[ $(g.version) == *"GRASS 7"* ]]; then
	grass_list="g.list"
	grass_mapset="g.mapset"
	grass_input="input"
	grass_vectremove="-f type=vector name"
	grass_addcolumn="v.db.addcolumn"
	grass_column="column"
	grass_other="other_"
	grass_separator="separator"
else
	grass_list="g.mlist"
	grass_mapset="g.mapsets"
	grass_input="dsn"
	grass_vectremove="vect"
	grass_addcolumn="v.db.addcol"
	grass_column="col"
	grass_other="o"
	grass_separator="fs"
fi

#First set working database type to SQL
db.connect driver=sqlite database="/home/$USER/grassdata/latlon/PERMANENT/sqlite.db"

#Set mapset to PERMANENT if not yet set
if [[ $($grass_mapset -p) != *"PERMANENT"* ]]; then
	g.mapset mapset=PERMANENT
fi

#Import Philippine Municipalities Shapefiles if not yet set
if [[ $($grass_list vect) != *"MuniCities"* ]]; then
	v.in.ogr $grass_input=grassdata/shapefiles/MuniCities.shp output=MuniCities
fi

#sample input
#cat <<- EOF > $temp_file.csv
#	119.239990000000,10.2095000000000
#	119.304367000000,10.2095000000000
#	119.304367000000,10.1465000000000
#	119.239990000000,10.1465000000000
#	119.239990000000,10.2725000000000
#EOF

cat <<- EOF > $temp_file.csvt
	Real(16.12),Real(16.13)
EOF

#Delete existing working tables
if [[ $(db.tables -p) == *"PolyPoints_csv"* ]]; then
	db.droptable table=PolyPoints_csv -f --quiet
fi
if [[ $($grass_list vect) == *"PolyPoints"* ]]; then
	g.remove $grass_vectremove=PolyPoints --quiet
fi

#Import stdin to Grass sql database
db.in.ogr $grass_input=$temp_file.csv output=PolyPoints_csv key=id --quiet

#Delete temporary file holders
rm $temp_file.csv
rm $temp_file.csvt

#Convert Grass database to vector map
v.in.db table=PolyPoints_csv x=field_1 y=field_2 key=id output=PolyPoints --quiet

#Add new column to vector map for municipality index
$grass_addcolumn map=PolyPoints columns="nrst_muni int" --quiet

#Find nearest municipality then place index to previously created column
v.distance from=PolyPoints to=MuniCities upload=cat column=nrst_muni to_type=area --quiet

#Merge sql databases 
v.db.join map=PolyPoints $grass_column=nrst_muni ${grass_other}table=MuniCities ${grass_other}col=cat

#Display output
# v.db.select map=PolyPoints

#output
v.out.ascii input=PolyPoints format=point columns=NAME_1,NAME_2 $grass_separator=, output=$out_file

#Remove working databases and maps
db.droptable table=PolyPoints_csv -f
g.remove $grass_vectremove=PolyPoints --quiet
