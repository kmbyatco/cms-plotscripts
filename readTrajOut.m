%Check output netcdf data from trajectory files to structure array
% function ncdata = readTrajOut( traj_filenames )
function ncdata = readTrajOut( varargin )
	if ( isOctave() == 0 )
		error("Not running inside octave")
	end
	
	switch length(varargin)
		case 3
			lat_extents = varargin{3};
			lon_extents = varargin{2};
			traj_filenames = varargin{1};
		case 1
			traj_filenames = varargin{1};
		otherwise
			error( "Wrong inputs in function" )
			return;
	endswitch

	pkg load netcdf

	startday=2455198; %%january 1 2010

	ncdata.lon = [];
	ncdata.lat = [];
	ncdata.datetime = [];
	ncdata.relpol = [];
	% ncdata.time = [];
	% ncdata.rel = [];
	% rel_range = [];
	ncdata.datetime_range = [];
	ncdata.exitcode = [];
	
	for j=1:size(traj_filenames)(1)
		printf( "%d of %d: Parsing %s\n", j, size(traj_filenames)(1), traj_filenames(j,:) );
		fflush(stdout);
		
		% open trajectory file and get values
		ncid = netcdf_open(traj_filenames(j,:),'NC_NOWRITE');
		
		varidLon = netcdf_inqVarID(ncid,'lon');
		lon = netcdf_getVar(ncid,varidLon);
		% ncdata.size{j,1} = size(lon);
		% fflush(stdout);
		
		varidLat = netcdf_inqVarID(ncid,'lat');
		lat = netcdf_getVar(ncid,varidLat);
		% ncdata.size{j,2} = size(lat);

		if( exist("lon_extents") && ~isempty( lon_extents ) &&
			exist("lat_extents") && ~isempty( lat_extents ) )
			
			location_index = all( [	any( bsxfun( @gt, lon, min(lon_extents) ) ) ;
									any( bsxfun( @lt, lon, max(lon_extents) ) ) ; 
									any( bsxfun( @gt, lat, min(lat_extents) ) ) ;
									any( bsxfun( @lt, lat, max(lat_extents) ) ) ; ] );
								
		else
			location_index = [1:(size(lon)(2))];
		endif		
		
		% printf( "\tAcquired lon: %d,%d\n", size(lon) );
		% printf( "\tAcquired lat: %d,%d\n", size(lat) );
		ncdata.lon = [ncdata.lon, lon(:, find(location_index))]; %lon must not be changed due to repmat code
		ncdata.lat = [ncdata.lat, lat(:, find(location_index))];
		% fflush(stdout);
		
		
		varidTime = netcdf_inqVarID(ncid,'time');
		time = netcdf_getVar(ncid,varidTime);
		time(time<0)=NaN;
		time = repmat(time,1,size(lon)(2));
		% ncdata.size{j,3} = size(time);
		% ncdata.time = [ ncdata.time, time ];
		
		% printf( "\tAcquired time: %d,%d\n", size(time) );
		% fflush(stdout);
		
		varidrel = netcdf_inqVarID(ncid,'releasedate');
		rel = netcdf_getVar(ncid,varidrel);
		rel -= startday;
		rel = 60*60*24*rel;
		rel = repmat(rel',size(lon)(1),1);
		% ncdata.size{j,4} = size(rel);
		% ncdata.rel = [ ncdata.rel, rel ];
		
		% printf( "\tAcquired release location: %d,%d\n", size(rel) );
		% fflush(stdout);
		
		% datetime = repmat(time,1,size(lon)(2)) + repmat(rel',size(lon)(1),1);
		datetime = time + rel;
		datetime = datetime(:, find(location_index));
		ncdata.datetime = [ ncdata.datetime, datetime ];
		
		% printf( "\tMerged date and release loaction data\n" );
		% fflush(stdout);
		
		ncdata.datetime_range = unique( [ ncdata.datetime_range; datetime(:)] );
		% rel_range = unique( [ rel_range; rel(:)] );
		
		% printf( "\tUpdated date and time index\n" );
		% fflush(stdout);
		
		varidexit = netcdf_inqVarID(ncid,'exitcode');
		exitcode = netcdf_getVar(ncid,varidexit);
		% ncdata.size{j,5} = size(exitcode);
		% total_exitcode += sum( exitcode==-4 );
		exitcode = repmat(exitcode',size(lon)(1),1);
		exitcode = exitcode(:, find(location_index));
		ncdata.exitcode = [ncdata.exitcode, exitcode];
		
		if( max( netcdf_inqVarIDs(ncid) ) >= 9 )	
			ifvarRel = 1;
			varidRelpol= netcdf_inqVarID(ncid,'releasepolygon');
			relpol = netcdf_getVar(ncid,varidRelpol);
			% ncdata.size{j,6} = size(relpol);
			relpol = repmat(relpol',size(lon)(1),1);
			relpol = relpol(:, find(location_index));
			
			ncdata.relpol = [ncdata.relpol, relpol];
			
			% printf( "\tAcquired release polygon: %d,%d\n", size(relpol) );
			% fflush(stdout);
		endif
		
		netcdf_close(ncid);

	endfor
	
	ncdata.lat(ncdata.lat>999) = NaN;
	ncdata.lon(ncdata.lon>999) = NaN;

endfunction