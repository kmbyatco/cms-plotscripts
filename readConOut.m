%read and compile CMS con_files from a list of filenames to a single variable
function condata = readConOut( varargin )
	
	switch length(varargin)
	case 2
		month_extents = varargin{2};
		con_filenames = varargin{1};
	case 1
		con_filenames = varargin{1};
	otherwise
		error( "Wrong inputs in function" )
		return;
	endswitch
	
	condata = [];
	for k=1:size(con_filenames)(1)
		% User stdout prompt
		printf( "%d of %d: Parsing %s\n", k, size(con_filenames)(1), con_filenames(k,:) );
		fflush(stdout);

		% check if directory exsits
		list = dir( con_filenames(k,:) );
		
		%skip if empty
		if( list.bytes == 0)
			continue;
		end
		
		%load the matlab readable files
		_con_file=load( con_filenames(k,:) );
		
		%compile to a single variable
		condata = [ condata ; _con_file];
	endfor
	
	% filter out null polygons
	condata( condata(:,1) == 0, : ) = [];
	
	if( exist("month_extents") && ~isempty( month_extents ) )
		time_index = all( horzcat( 	bsxfun( @ge, condata(:,4), min(month_extents) ) ,
									bsxfun( @le, condata(:,4), max(month_extents) ) )' )';
		condata = condata( time_index, : );
	endif	
	
endfunction