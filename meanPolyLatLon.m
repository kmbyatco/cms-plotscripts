% mean function for lat lon pairs grouped by polygon which may have empty of missing pairs
function output = meanPolyLatLon( lon, lat, poly )

	% check if averaging between polygons is possible	
	try
		%get polygon centers
		output.lon = mean(reshape(lon,[],1,max(poly)))(:);
		output.lat = mean(reshape(lat,[],1,max(poly)))(:);
	catch
		warning("Can't quickly compute lon lat centers by polygon, iterating...");
		output.lon=[];
		output.lat=[];
		for i = 1 : max(poly)
		
			if( isempty(lon(poly==i)) )
				
				mean_lon = 0;
				mean_lat = 0;
			endif
			
			if( isempty( lon(poly==i) ) ||  
				isempty( lon(poly==i) )	)
				mean_lon = 0;
				mean_lat = 0;
			else
				mean_lon = mean( lon(poly==i) );
				mean_lat = mean( lat(poly==i) );
			endif
			
			if( isnan(mean_lon) || isnan(mean_lat) )
				mean_lon = 0;
				mean_lat = 0;
			endif
			
			output.lon = [ output.lon; mean_lon ];
			output.lat = [ output.lat; mean_lat ];
		endfor
	end_try_catch
	
endfunction
