%clc;
%clear;

pkg load mapping;
pkg load statistics;
format long;

function c = outhaversine( x, y )
	[a,c,dlat,dlon]=haversine(x(:,1),x(:,2),y(1,:),y(2,:));
end;

function rad = radians(degree) 
% degrees to radians
    rad = degree .* pi / 180;
end; 
 
%% outputs distance in meters between two lon-lan points
function [dlength,dlat,dlon]=haversine(latlon1,latlon2)
    dlat = radians( bsxfun(@minus, latlon2(:,1), latlon1(:,1)') );
    dlon = radians( bsxfun(@minus, latlon2(:,2), latlon1(:,2)') );
    lat1 = radians(latlon1(:,1));
    lat2 = radians(latlon2(:,1));
	a = bsxfun(@times, cos(lat1), cos(lat2)');
	b = bsxfun(@times, a, ((sin(dlon./2)).^2)');
    c = (sin(dlat./2)).^2 + b';
    d = 2 .* asin(sqrt(c));
    dlength = 6372.8.*d.*1000;
    %arrayfun(@(x) printf("distance: %.4f m\n",x), dlength);
end;

function count = countlinks( xyl, cutoff )
	_count = arrayfun( @(x) find( xyl(:,3,:)<x ), cutoff, "UniformOutput", false );
	count = cellfun( @length, _count );
end;

%can't remember where the source of this is, basically creates cluster grouping indices
function cls =  cluster( xyl, cutoff )
	i = [1:cutoff];
	dclusters = countlinks( xyl, i ) - countlinks( xyl, i-1 );
	nelements = length(xyl);
	clusters = (nelements+1-cumsum(dclusters))(end);

	cls = zeros(nelements+1, 1 );
	cMgroups = [ 1+(nelements+1) : (nelements+1) - clusters + (nelements+1) ];
	i=0;

	while( length(cMgroups) > 0 )
		i++;
		cmembers = [];
		cmgroups = [];
		cgroups = [];
		cgroups(end+1) = cMgroups(end); 
		cmgroups(end+1) = cMgroups(end); 

		while(  length(cgroups) >  0 )
	
			cend =  cgroups(end); 
			cindex = xyl( cend - (nelements+1) , 1:2, : );
			if( cindex(1) <= (nelements+1) )
				cmembers(end+1) = cindex(1);
			else
				cmgroups(end+1) = cindex(1);
				cgroups(end+1) = cindex(1);
			endif 
			
			if( cindex(2) <= (nelements+1) )
				cmembers(end+1) = cindex(2);
			else
				cmgroups(end+1) = cindex(2);
				cgroups(end+1) = cindex(2);
			endif
		
			cgroups( cgroups==cend ) = [];
	
		endwhile

		cMgroups( ismember(cMgroups,cmgroups) ) = [];
		cls(cmembers)=i;

	endwhile

end;

function createXYG( filename, maxCircle )

        xyd =  csvread( filename );

        %maxCircle = 0.06;
        %maxCircle = 0.1;
        minCircle = 0.03;

        circleSize = ( log10( xyd(headerrow+1:end,3)+1 ) ) * (maxCircle - minCircle) + minCircle;
        colorValue = floor ( ( xyd(headerrow+1:end,3)/100 ) * 360 );

        onesrow=ones( size(colorValue)(1),1 );
        csvwrite( [filename ".xyg"], [xyd(headerrow+1:end,1:2),circleSize] );

        return;
endfunction

%% finds lon-lat grouping centers given by radius cdistance, best to work with arrays < 1000
function [xyg,cls] =  groupXYD( xy, cdistance )

	%get distances between all points
	[dlength,dlat,dlon]=haversine( xy(:,:), xy(:,:) );
	dlength( isnan(dlength) ) = 2^100;
	dlength(logical(eye(size(dlength)))) = 0; 
	
	%generate linkage array from sequence of distances of al points
	xyl = linkage( squareform(dlength), "weighted" );
	
	cls = cluster( xyl, cdistance ); %% cls is index of cluster ids relative to lon&lat index
	
	% identify isolated groupings and tag them appropriately as the last member of cls
	_cls = cls ;
	_cls(cls~=0) += length(cls(cls==0));
	_cls(cls==0) = 1:length( cls(cls==0) );
	cls = _cls;
	
	% allocate xyg size by adding clusters and previously generated no-cluster groupings
	_xyg = [xy,cls];
	
	% xyg columns are = lon,lat,reduced cluster id,number of members
	xyg = zeros( max(cls), 4 );
	for i = 1:max(cls);
		xyg(i,1:3) = mean( _xyg(_xyg(:,3)==i, :), 1 );
		xyg(i,4) = length( _xyg(_xyg(:,3)==i, :) );
	endfor
	
	
	% csvwrite( [filename ".xyg"], xyg(:,1:2) );
end;

function [lonlat1, lonlat2, lonlatstring] = getsquare( lonlat, area )

	y2deg = 1/111111;
	x2deg = 1/( 111111*cos( lonlat(2)*pi/180 ) );

	d=(sqrt(area)/2);
    
	lat1 = lonlat(2) + (y2deg*d);
	lon1 = lonlat(1) + (x2deg*d);

	lat2 = lonlat(2) - (y2deg*d);
	lon2 = lonlat(1) - (x2deg*d);

	lonlat1 = [lon1, lat1];
	lonlat2= [lon2, lat1];
	lonlatstring=sprintf( "%f/%f/%f/%f\n", min(lon1,lon2), max(lon1,lon2), min(lat1,lat2), max(lat1,lat2) );    
    
end

