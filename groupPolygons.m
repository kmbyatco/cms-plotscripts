% groups polygon centers from the experiment poly xyz file into clusters defined by a raduis of cluster_distance
function [xy_grouping,cluster_assignments, cluster_distance] = groupPolygons( varargin )

	switch length(varargin)
		case 5
			custom_locations = varargin{5};
			lat_extents = varargin{4};
			lon_extents = varargin{3};
			cluster_distance = varargin{2};
			polygon_filename = varargin{1};
		case 4
			lat_extents = varargin{4};
			lon_extents = varargin{3};
			cluster_distance = varargin{2};
			polygon_filename = varargin{1};
		case 2
			cluster_distance = varargin{2};
			polygon_filename = varargin{1};
		otherwise
			error( "Wrong inputs in function" )
			return;
	endswitch

	%load clustering functions
	groupXY; 

	%parse polygon coordinates
	fid = fopen(polygon_filename);
	_polygon_xyz = textscan( fid, '%f %f %d', 'Delimiter', ' ');
	fclose(fid);
	polygon_xyz.lon = _polygon_xyz{1};
	polygon_xyz.lat = _polygon_xyz{2};
	polygon_xyz.poly = _polygon_xyz{3};
	
	
	if( exist("lon_extents") && ~isempty( lon_extents ) &&
		exist("lat_extents") && ~isempty( lat_extents ) )
		
		location_index = all( horzcat(	bsxfun( @gt, polygon_xyz.lon, min(lon_extents) ),
										bsxfun( @lt, polygon_xyz.lon, max(lon_extents) ),
										bsxfun( @gt, polygon_xyz.lat, min(lat_extents) ),
										bsxfun( @lt, polygon_xyz.lat, max(lat_extents) ))' )';
		polygon_xyz.lon = polygon_xyz.lon(location_index);
		polygon_xyz.lat = polygon_xyz.lat(location_index);
		polygon_xyz.poly = polygon_xyz.poly(location_index);
		
		if( exist("custom_locations") && ~isempty( custom_locations ) )
			location_index = all( horzcat(	bsxfun( @gt, custom_locations(:,1), min(lon_extents) ),
											bsxfun( @lt, custom_locations(:,1), max(lon_extents) ),
											bsxfun( @gt, custom_locations(:,2), min(lat_extents) ),
											bsxfun( @lt, custom_locations(:,2), max(lat_extents) ))' )';
											
			custom_locations = custom_locations(location_index,:);
		endif
		
	endif	
	
	if( exist("custom_locations") && ~isempty( custom_locations ) )
		printf( "Overidding set cluster_distance due to custom_location distances\n")
		fflush(stdout);
	
		% get shortest distance between two points, 2nd via haversine function due to 0 distance on same points
		custom_cluster_distance = unique(haversine( custom_locations, custom_locations ))(2);
		
		cluster_distance = min( [cluster_distance, custom_cluster_distance] );
		
		%get matrix of distance between the polygon points and the custom_locations
		custom_distance_matrix = haversine( custom_locations, [ polygon_xyz.lon, polygon_xyz.lat ] );
		[custom_sub,] = ind2sub( size(custom_distance_matrix), find(custom_distance_matrix < cluster_distance ) )';
		
		
		% sort by polygon value
		custom_poly = unique(polygon_xyz.poly(custom_sub));
		custom_index = any(bsxfun( @eq, polygon_xyz.poly', custom_poly) );
	
		% cluster polygon centers for custom location
		custom_cluster = meanPolyLatLon(	polygon_xyz.lon( custom_index ),
											polygon_xyz.lat( custom_index ),
											polygon_xyz.poly( custom_index ) );
		
		% remove empty groupings due to nature of max(poly) on meanPolyLatLon
		custom_cluster.lon( custom_cluster.lon == 0 ) = [];
		custom_cluster.lat( custom_cluster.lat == 0 ) = [];
		
		[custom_xy_grouping, custom_cluster_assignments] = groupXYD( [ custom_cluster.lon, custom_cluster.lat ], cluster_distance );
		
		polygon_xyz.lon(custom_index) = 0;
		polygon_xyz.lat(custom_index) = 0;
	endif
	
	cluster = meanPolyLatLon( polygon_xyz.lon, polygon_xyz.lat, polygon_xyz.poly );
	
	%cluster polygon centers
	[xy_grouping,cluster_assignments] = groupXYD( [cluster.lon, cluster.lat], cluster_distance );
	
	if( exist("custom_locations") && ~isempty( custom_locations ) )
		old_max = xy_grouping(xy_grouping(:,1) == 0, 3 );
		new_max = size([xy_grouping; custom_xy_grouping])(1);
		
		cluster_assignments(cluster_assignments==old_max) = new_max;
		xy_grouping(xy_grouping(:,1) == 0, 3 ) = new_max;
	
		custom_xy_grouping(:,3) = custom_xy_grouping(:,3) + old_max-1;
		custom_cluster_assignments = custom_cluster_assignments + old_max-1;
		
		% subrtract custom location centers from true zeros
		xy_grouping(xy_grouping(:,1) == 0, 4 ) = sum(custom_xy_grouping(:,4)) - xy_grouping(xy_grouping(:,1) == 0, 4 ) ;

		cluster_assignments(custom_poly) = custom_cluster_assignments; %assuming custom_poly value is same as custom_poly index
		xy_grouping = [xy_grouping(1:end-1,:) ; custom_xy_grouping ; xy_grouping(end,:)];
	endif 
	
endfunction
